package com.Week15_GL_oto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.Week15_GL_oto.entity.ID;
import com.Week15_GL_oto.entity.Person;
import com.Week15_GL_oto.repository.RepositoryForPerson;


@SpringBootApplication
public class Week15GlOtoApplication implements CommandLineRunner{
	
	@Autowired
    private RepositoryForPerson personRepository;

	public static void main(String[] args) {
		SpringApplication.run(Week15GlOtoApplication.class, args);
	}
	@Override
    public void run(String... strings) throws Exception {
        // save idCard along with persons
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("Sai Kiran", new ID()));
        persons.add(new Person("Sam", new ID()));
        persons.add(new Person("Newton", new ID()));
        persons.add(new Person("Aliya", new ID()));
        personRepository.saveAll(persons);
    }
}
